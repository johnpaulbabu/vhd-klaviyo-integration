# Klaviyo and VHD Integration Plan

**Author:** John Paul  
**Email:** john@jondevops.dev  
**Date:** November 10, 2023

## Overview

This document outlines the integration plan for synchronizing data between Klaviyo (App 1) and VHD (App 2). The integration involves profile and segment synchronization, considering the static nature of Klaviyo Lists and the dynamic growth of Segments in real-time.

## Profile Synchronization

1. **Fetch Profiles from Klaviyo (App 1):**

   - API: `GET {{baseUrl}}/api/profiles/`
   - Utilize the Klaviyo API to retrieve all user profiles.

2. **Check for Duplicates in VHD (App 2):**

   - API: `GET {{baseUrl}}/people/2/persons`
   - Use App 2 API to fetch existing profiles.
   - Compare profiles from App 1 with existing profiles in App 2 based on a unique identifier (e.g., email or phone_number).
   - Send only new profiles to App 2.

3. **Create Batch People in App 2:**

   - API: `POST {{baseUrl}}/people/2/persons/batch`
   - Use App 2 API to create a batch of people for new profiles from Klaviyo.

4. **Update People in App 2:**
   - Implement periodic synchronization to update people in App 2 with the latest profile data from App 1.
   - API: `PUT {{baseUrl}}/people/2/persons/batch`

## Segment Synchronization

1. **Fetch Segments from Klaviyo (App 1):**

   - API: `GET {{baseUrl}}/api/segments/`
   - Use Klaviyo API to retrieve all segments.

2. **Check for Existing Segments in VHD (App 2):**

   - API: `GET {{baseUrl}}/people/2/tags`
   - Use App 2 API to fetch existing tags (segments).
   - Compare segments from App 1 with existing tags in App 2 based on a unique identifier (e.g., segment name).
   - Send only new segments to App 2.

3. **Create Tags (Segments) in App 2:**

   - API: `POST {{baseUrl}}/people/2/tags`
   - Use App 2 API to create segments for new segments from Klaviyo.

4. **Attach Tags to People in App 2:**
   - API: `PATCH {{baseUrl}}/people/2/tags/{{name}}/persons`
   - For each segment created in App 2, batch attach the segment to corresponding profiles in App 2 based on Klaviyo's segment conditions.

## Additional Considerations

- **Error Handling:**

  - Implement robust error handling for API requests in case of unavailability or errors.

- **Secure Credential Storage:**

  - Utilize secure methods for storing and handling API keys and credentials.

- **Scheduled Synchronization:**

  - Schedule periodic synchronization tasks to ensure consistent data between Klaviyo and App 2.

- **User Interface:**
  - Develop a user interface allowing users to manually initiate synchronization, manage accounts, and configure settings.

## Backend

- Develop backend logic for API requests, data synchronization, and user management.
- Implement secure API endpoints for data retrieval and updates.
- Define user roles such as Super Admin, Organization Admin, and Regular User.

## Database Design

- Design the database schema to store user profiles, segments, and other relevant data.
- Ensure proper indexing for efficient data retrieval.
- Choose a suitable database system for the project's requirements.

## Logging and Auditing

- Log synchronization activities for auditing and troubleshooting purposes.

## Testing

- Develop unit tests, integration tests, and end-to-end tests.
- Perform thorough testing of the synchronization process and critical functionalities.
- Conduct user acceptance testing (UAT) with stakeholders.

## DevOps

- Set up a version control system (e.g., Git) for collaboration.
- Implement CI/CD pipelines for automated testing and deployment.
- Configure and manage server infrastructure.

## Post Deployment Maintenance

- Establish a maintenance plan for ongoing support, bug fixes, and updates.
- Regularly monitor system performance and address issues promptly.

## Klaviyo List and Segment Dynamics

- Understand that Klaviyo Lists are static, while Segments are dynamic and grow in real-time.
- Note that Klaviyo does not provide webhooks to notify changes in segments, lists, or profiles.

## Authentication

For all API requests to App 1 (Klaviyo) and App 2 (VHD), we use API keys for authorization.

## Challenges 

- **Temporary Storage vs. Permanent Database:**
  - Consider using temporary storage with memory caching for comparison and updates or opt for a permanent database for long-term storage.

- **Rate Limiting:**
  - Acknowledge rate limits in both Klaviyo and App 2 APIs; plan synchronization frequency accordingly.

- **Lack of Webhook Support in Klaviyo:**
  - Note that Klaviyo does not provide webhooks to notify changes in segments, lists, or profiles. This absence of real-time event notifications poses a challenge in tracking changes instantly.

- **Multi-Account Architecture:**
  - Design an architecture to handle multiple Klaviyo and VHD accounts and route traffic to the correct accounts.


## Conclusion

Implementing this integration plan requires careful consideration of API interactions, rate limits, data storage, and user experience. Regular testing and monitoring will be essential to maintain a seamless data flow between Klaviyo and App 2 while adhering to GDPR regulations and best practices.

### Example API Requests

#### Klaviyo API

1. **Get Profiles**

   - **API:** `{{baseUrl}}/api/profiles/`
   - **Response:**
     ```json
     {
       "data": [
         {
           "type": "profile",
           "id": "01GF617D5ANDHSGH9VX5YMW4J5",
           "attributes": {
             "email": "johnpaul@irisind.com",
             "phone_number": null,
             "first_name": null,
             "last_name": null,
             "organization": null,
             "title": null,
             "created": "2022-10-12T12:03:07+00:00",
             "updated": "2022-10-12T12:03:07+00:00",
             "last_event_date": "2022-10-12T12:03:07+00:00",
             "location": {
               "address1": null,
               "address2": null,
               "city": null,
               "country": null,
               "latitude": null,
               "longitude": null,
               "region": null,
               "zip": null,
               "timezone": null,
               "ip": null
             },
             "properties": {}
           },
           "relationships": {
             "lists": {
               "links": {
                 "self": "https://a.klaviyo.com/api/profiles/01GF617D5ANDHSGH9VX5YMW4J5/relationships/lists/",
                 "related": "https://a.klaviyo.com/api/profiles/01GF617D5ANDHSGH9VX5YMW4J5/lists/"
               }
             },
             "segments": {
               "links": {
                 "self": "https://a.klaviyo.com/api/profiles/01GF617D5ANDHSGH9VX5YMW4J5/relationships/segments/",
                 "related": "https://a.klaviyo.com/api/profiles/01GF617D5ANDHSGH9VX5YMW4J5/segments/"
               }
             }
           },
           "links": {
             "self": "https://a.klaviyo.com/api/profiles/01GF617D5ANDHSGH9VX5YMW4J5/"
           }
         }
         // Additional profiles...
       ]
     }
     ```

2. **Get Profile Segments**
   - **API:** `{{baseUrl}}/api/profiles/01GF632CCZSYVP33W8T4JWE80A/segments/`
   - **Response:**
     ```json
     {
       "data": [
         {
           "type": "segment",
           "id": "TC3Rn6",
           "attributes": {
             "name": "music",
             "created": "2023-11-06T21:45:09+00:00",
             "updated": "2023-11-06T21:45:50+00:00"
           },
           "relationships": {
             "profiles": {
               "links": {
                 "self": "https://a.klaviyo.com/api/segments/TC3Rn6/relationships/profiles/",
                 "related": "https://a.klaviyo.com/api/segments/TC3Rn6/profiles/"
               }
             },
             "tags": {
               "links": {
                 "self": "https://a.klaviyo.com/api/segments/TC3Rn6/relationships/tags/",
                 "related": "https://a.klaviyo.com/api/segments/TC3Rn6/tags/"
               }
             }
           },
           "links": {
             "self": "https://a.klaviyo.com/api/segments/TC3Rn6/"
           }
         }
         // Additional segments...
       ],
       "links": {
         "self": "https://a.klaviyo.com/api/profiles/01GF632CCZSYVP33W8T4JWE80A/segments/",
         "next": null,
         "prev": null
       }
     }
     ```

3.**Get Segments**

- **API:** `{{baseUrl}}/api/segments/`
- **Response:**

```json
{
  "data": [
    {
      "type": "segment",
      "id": "TC3Rn6",
      "attributes": {
        "name": "music",
        "created": "2023-11-06T21:45:09+00:00",
        "updated": "2023-11-06T21:45:50+00:00"
      },
      "relationships": {
        "profiles": {
          "links": {
            "self": "https://a.klaviyo.com/api/segments/TC3Rn6/relationships/profiles/",
            "related": "https://a.klaviyo.com/api/segments/TC3Rn6/profiles/"
          }
        },
        "tags": {
          "links": {
            "self": "https://a.klaviyo.com/api/segments/TC3Rn6/relationships/tags/",
            "related": "https://a.klaviyo.com/api/segments/TC3Rn6/tags/"
          }
        }
      },
      "links": {
        "self": "https://a.klaviyo.com/api/segments/TC3Rn6/"
      }
    },
    {
      "type": "segment",
      "id": "U7CYvW",
      "attributes": {
        "name": "New Subscribers",
        "created": "2022-10-12T12:03:07+00:00",
        "updated": "2023-10-17T17:02:02+00:00"
      },
      "relationships": {
        "profiles": {
          "links": {
            "self": "https://a.klaviyo.com/api/segments/U7CYvW/relationships/profiles/",
            "related": "https://a.klaviyo.com/api/segments/U7CYvW/profiles/"
          }
        },
        "tags": {
          "links": {
            "self": "https://a.klaviyo.com/api/segments/U7CYvW/relationships/tags/",
            "related": "https://a.klaviyo.com/api/segments/U7CYvW/tags/"
          }
        }
      },
      "links": {
        "self": "https://a.klaviyo.com/api/segments/U7CYvW/"
      }
    }
  ],
  "links": {
    "self": "https://a.klaviyo.com/api/segments/",
    "next": null,
    "prev": null
  }
}
```

#### VHD API

1.**Batch People Create**

- **API:** `https://go2.vhdmedia.se/people/2/persons/batch`
- **Request:**

```json
{
  "people": [
    {
      "firstName": "Jane",
      "lastName": "Smith",
      "customAttributes": {
        "Contract Expiry": "2023-06-01",
        "Company": "Example",
        "ShoppingCartList": [
          {
            "productName": "Sneakers",
            "productPrice": 25.33,
            "productCategory": "Sport Sneakers",
            "productImage": "/image1.png"
          },
          {
            "productName": "T-Shirt",
            "productPrice": 9.99,
            "productCategory": "Casual",
            "productImage": "/image2.png"
          }
        ]
      },
      "contactInformation": {
        "email": [
          {
            "address": "janem@example.com"
          }
        ]
      }
    },
    {
      "firstName": "Jane",
      "lastName": "Will",
      "customAttributes": {
        "Contract Expiry": "2023-06-01",
        "Company": "Acme",
        "ShoppingCartList": [
          {
            "productName": "Sneakers",
            "productPrice": 25.33,
            "productCategory": "Sport Sneakers",
            "productImage": "/image1.png"
          },
          {
            "productName": "T-Shirt",
            "productPrice": 9.99,
            "productCategory": "Casual",
            "productImage": "/image2.png"
          }
        ]
      },
      "contactInformation": {
        "email": [
          {
            "address": "janewil@example.com"
          }
        ]
      }
    }
  ]
}
```

#### VHD API - Tags Operations

2.**Get Tags**

- **API:** `https://go2.vhdmedia.se/people/2/tags`
- **Response:**

```json
{
  "limit": 100,
  "page": 1,
  "orderBy": "name:asc, id:asc",
  "tags": [
    {
      "createdAt": "2023-05-17T12:10:04",
      "modifiedAt": "2023-05-17T12:10:04",
      "name": "New Customers"
    },
    {
      "createdAt": "2023-05-16T11:33:06",
      "modifiedAt": "2023-05-16T11:33:06",
      "name": "SOS"
    },
    {
      "createdAt": "2023-05-17T12:10:04",
      "modifiedAt": "2023-05-17T12:10:04",
      "name": "VIP Customers"
    },
    {
      "createdAt": "2023-05-19T14:51:42",
      "modifiedAt": "2023-05-19T14:51:42",
      "name": "Vip"
    }
  ]
}
```

3. **Create New Tag**

- **API:** `POST https://go2.vhdmedia.se/people/2/tags`
- **Body:**

```json
{
  "name": "Vip"
}
```

4. **Add Tag to People**

- **API:** `PATCH https://go2.vhdmedia.se/people/2/tags/ShoppingCartList/persons`
- **Body:**

```json
{
  "people": [
    {
      "query": {
        "email": "johndoe@example.com"
      }
    },
    {
      "query": {
        "email": "janesm@example.com"
      }
    }
  ]
}
```

